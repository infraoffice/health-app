import {

} from '../actions/types';

const initialState = {
  // Select Language
  // #################################################################################################################

  lets_start: { EN: "Let’s get started", AR: "هيا بنا نبدأ", UR: "آو شروع کریں", BN: "চল শুরু করি" },
  select_lang: { EN: "Please select your language", AR: "الرجاء اختيار لغتك", UR: "برائے کرم اپنی زبان منتخب کریں", BN: "আপনার ভাষা নির্বাচন করুন" },
  continue: { EN: "CONTINUE", AR: "استمر", UR: "جاری رہے", BN: "চালিয়ে যান" },
  change_lang: { EN: "Change Language", AR: "تغيير اللغة", UR: "زبان تبدیل کریں", BN: "ভাষা পরিবর্তন করুন" },

  // MObile verification
  // ###################################################################################################################
  verify_mobile: { EN: "Verify number", AR: "تحقق من الرقم", UR: "نمبر کی تصدیق کریں", BN: "নম্বর যাচাই করুন" },
  two_step_verification: {
    EN: "Enter your Mobile number to enable 2-step verification",
    AR: "أدخل رقم هاتفك المحمول إلى تمكين التحقق بخطوتين",
    UR: "اپنا موبائل نمبر درج کریں 2 قدمی توثیق کو فعال کریں",
    BN: "আপনার মোবাইল নম্বরটি প্রবেশ করুন ২-পদক্ষেপ যাচাইকরণ সক্ষম করুন"
  },

  // OTP Verification
  // #################################################################################################################
  verification: { EN: "Verification", AR: "التحقق", UR: "تصدیق", BN: "প্রতিপাদন" },
  verification_msg: {
    EN: "We texted you a code to verify your phone number",
    AR: "لقد أرسلنا لك رمزًا للتحقق رقم تليفونك",
    UR: "ہم نے توثیق کرنے کے لئے آپ کو ایک کوڈ ٹیکسٹ کیا آپ کا فون نمبر",
    BN: "আমরা আপনাকে যাচাই করার জন্য একটি কোড টেক্সট করেছি আপনার ফোন নম্বর"
  },
  did_not_receive: {
    EN: "I did not receive a code?",
    AR: "لم أتسلم الشفرة؟",
    UR: "مجھے کوڈ نہیں ملا؟",
    BN: "আমি কোন কোড পাই নি?"
  },
  resend: { EN: "Resend", AR: "إعادة إرسال", UR: "دوبارہ بھیجیں", BN: "পুনরায় পাঠান" },

  // Notification Screen
  // ##################################################################################################################
  notification: { EN: "Notification", AR: "تنبيه", UR: "اطلاع", BN: "বিজ্ঞপ্তি" },
  notification_msg: {
    EN: "Stay notified about to Alert and remind the booking",
    AR: "ابق على اطلاع حول التنبيه وتذكير الحجز",
    UR: "انتباہ کے بارے میں مطلع رہیں اور بکنگ کو یاد دلائیں",
    BN: "সতর্কতা সম্পর্কে অবহিত থাকুন এবং বুকিংয়ের কথা মনে করিয়ে দিন"
  },
  allow: { EN: "ALLOW", AR: "السماح", UR: "سب کچھ", BN: "সব" },
  skip: { EN: "SKIP", AR: "تخطى", UR: "چھوڑ دو", BN: "স্কিপ" },


  // Login screen text
  // ################################################################################################################
  sign_in: { EN: "Sign In", AR: "تسجيل الدخول", UR: "سائن ان", BN: "সাইন ইন করুন" },
  login: { EN: "Log in", AR: "تسجيل الدخول", UR: "لاگ ان کریں", BN: "প্রবেশ করুন" },
  login_msg: {
    EN: "Enter your login details to access\nyour account",
    AR: "أدخل تفاصيل تسجيل الدخول الخاصة بك للوصول الحساب الخاص بك",
    UR: "رسائی کے ل your اپنے لاگ ان کی تفصیلات درج کریں آپ کا کھاتہ",
    BN: "অ্যাক্সেস করতে আপনার লগইন বিশদ লিখুন আপনার অ্যাকাউন্ট"
  },
  login_btn_txt: { EN: "LOGIN", AR: "تسجيل الدخول", UR: "لاگ ان کریں", BN: "প্রবেশ করুন" },
  mobile_number: { EN: "Mobile Number", AR: "تسجيل الدخول", UR: "سائن ان", BN: "সাইন ইন করুন" },
  mobile_placeholder: { EN: "Enter Mobile Number", AR: "أدخل رقم الهاتف المتحرك", UR: "موبائل نمبر درج کریں", BN: "মোবাইল নম্বর প্রবেশ করান" },
  password: { EN: "Enter Password", AR: "أدخل كلمة المرور", UR: "پاس ورڈ درج کریں", BN: "পাসওয়ার্ড লিখুন" },
  forget_password: { EN: "Forgot Password?", AR: "هل نسيت كلمة المرور؟", UR: "پاس ورڈ بھول جانا؟", BN: "পাসওয়ার্ড ভুলে গেছেন?" },
  or: { EN: "OR", AR: "أو", UR: "یا", BN: "বা" },
  create_account: { EN: "Create an account", AR: "انشئ حساب", UR: "کھاتا کھولیں", BN: "একটি অ্যাকাউন্ট তৈরি করুন" },
  show: { EN: "SHOW", AR: "تبين", UR: "دکھائیں", BN: "দেখান" },
  hide: { EN: "HIDE", AR: "إخفاء", UR: "چھپائیں", BN: "লুকান" },
  do_not_have_acc: { EN: "Don’t have an Account?", AR: "ليس لديك حساب؟", UR: "کیا آپ کا اکاؤنٹ نہیں ہے؟", BN: "একটি অ্যাকাউন্ট নেই?" },





  // Registration screen text
  // #############################################################################################################3
  back: { EN: "Back", AR: "عودة", UR: "پیچھے", BN: "পেছনে" },
  registration: { EN: "Registration", AR: "التسجيل", UR: "اندراج", BN: "নিবন্ধন" },
  registration_msg: {
    EN: "Enter your details to create your account",
    AR: "أدخل التفاصيل الخاصة بك لإنشاء الحساب الخاص بك",
    UR: "تیار کرنے کے لئے اپنی تفصیلات درج کریں آپ کا کھاتہ",
    BN: "তৈরি করতে আপনার বিশদ লিখুন আপনার অ্যাকাউন্ট"
  },
  name: { EN: "Enter your name", AR: "أدخل أسمك", UR: "اپنا نام درج کریں", BN: "আপনার নাম প্রবেশ করুন" },
  cpr_number: { EN: "CPR Number", AR: "رقم البطاقة السكانية", UR: "سی پی آر نمبر", BN: "সিপিআর নম্বর" },
  re_enter_assword: { EN: "Re-Enter Password", AR: "إعادة إدخال كلمة المرور", UR: "پاس ورڈ دوبارہ درج", BN: "পাসওয়ার্ড পুনরায় প্রবেশ" },


  // Profile registration
  // ###################################################################################################################

  profile: { EN: "Profile", AR: "الملف الشخصي", UR: "پروفائل", BN: "প্রোফাইল" },
  address: { EN: "Address", AR: "عنوان", UR: "پتہ", BN: "ঠিকানা" },
  area: { EN: "Area", AR: "منطقة", UR: "رقبہ", BN: "ক্ষেত্রফল" },
  block: { EN: "Block", AR: "منع", UR: "بلاک کریں", BN: "ব্লক" },
  road: { EN: "Road", AR: "الطريق", UR: "روڈ", BN: "রাস্তা" },
  building_villa: { EN: "Building/Villa", AR: "بناء / فيلا", UR: "عمارت / ولا", BN: "বিল্ডিং / ভিলা" },
  term_and_condition: { EN: "Agree to Term and Condition", AR: "توافق على الشروط والاحكام", UR: "مدت اور حالت سے اتفاق کریں", BN: "শর্ত ও শর্তে সম্মত হন" },
  register: { EN: "Register", AR: "تسجيل", UR: "رجسٹر کریں", BN: "নিবন্ধন" },
  already_have_acc: { EN: "Already have an Account?", AR: "هل لديك حساب؟", UR: "پہلے سے ہی ایک اکاؤنٹ ہے؟", BN: "ইতিমধ্যে একটি সদস্যপদ আছে?" },


  //Home screen text 
  // #################################################################################################################
  near_by: { EN: "Near by", AR: "مجاور", UR: "قریب ہی", BN: "কাছাকাছি" },
  masjid_near_me: { EN: "Masjid near me |", AR: "مسجد بالقرب مني |", UR: "میرے قریب مسجد |", BN: "আমার কাছে মসজিদ |" },
  booking_cancel_reason: { EN: "Booking cancelled by following reason :", AR: "تم إلغاء الحجز للسبب التالي:", UR: "مندرجہ ذیل وجہ سے بکنگ منسوخ ہوگئی:", BN: "নিম্নলিখিত কারণে বুকিং বাতিল হয়েছে:" },
  book_again: { EN: "Don’t worry you can still book from your near by Mosque", AR: "لا تقلق ، لا يزال بإمكانك الحجز من المسجد القريب", UR: "پریشان ہونے کی کوئی بات نہیں کہ آپ ابھی بھی اپنے قریب سے مسجد کے ذریعے بکنگ کرسکتے ہیں", BN: "চিন্তা করবেন না যে আপনি এখনও মসজিদ দ্বারা আপনার কাছাকাছি থেকে বুক করতে পারেন" },
  book_your_location: { EN: "Book your location", AR: "احجز موقعك", UR: "اپنا مقام بک کرو", BN: "আপনার অবস্থান বুক করুন" },
  available: { EN: "Available", AR: "متاح", UR: "دستیاب", BN: "উপলব্ধ" },
  booked: { EN: "Booked", AR: "حجز", UR: "بک کیا", BN: "বুক করা" },
  book_now: { EN: "Book Now", AR: "احجز الآن", UR: "ابھی کتاب کرو", BN: "এখনই বুক করুন" },
  full_booked: { EN: "Full Booked", AR: "محجوزة بالكامل", UR: "مکمل بک", BN: "পুরো বুকড" },
  cancel_booking: { EN: "Cancel booking", AR: "إلغاء الحجز", UR: "بکنگ منسوخ کریں", BN: "বুকিং বাতিল করুন" },
  navigate_now: { EN: "Navigate now", AR: "انتقل الآن", UR: "ابھی تشریف لے جائیں", BN: "এখনই নেভিগেট করুন" },
  you_have_booked: { EN: "You have booked !", AR: "لقد حجزت!", UR: "آپ نے بک کیا ہے!", BN: "আপনি বুক করেছেন!" },




  next_juma_on: { EN: "Next Juma’a On", AR: "جمعة التالي", UR: "اگلا جمعہ آن", BN: "পরবর্তী জুম'আ চালু" },
  masjid_in: { EN: "Masjid in", AR: "مسجد في", UR: "مسجد اندر", BN: "মসজিদে" },
  masjid_address: { EN: "Masjid Address", AR: "عنوان المسجد", UR: "مسجد کا پتہ", BN: "মসজিদের ঠিকানা" },

  my_booking: { EN: "My Booking", AR: "الحجز", UR: "میری بکنگ", BN: "আমার বুকিং" },
  conf_message: {
    EN: "You are about to book a location in [set masjid name] For Juma’a Salah on [ set date]",
    AR: "أنت على وشك حجز أ موقع في [تعيين اسم المسجد] جمعة صلاح يوم [تحديد التاريخ]",
    UR: "آپ بک کرنے والے ہیں [سیٹ مسجد نام] میں مقام جمعہ صلاح کیلئے [تاریخ طے شدہ]",
    BN: "আপনি একটি বুক করতে চলেছেন [সেট মসজিদের নাম] এ অবস্থান জুম'আ সালাহ চালু আছে [ তারিখ ঠিক করা]"
  },
  agreement_message: {
    EN: "I Confirm taking all precautions for social distancing, and the published rules of Sunni Awqaf for Juma’a Salah.",
    AR: "أؤكد اتخاذ جميع الاحتياطات من أجل التباعد الاجتماعي ، و نشر قواعد الاوقاف السنية جمعة صلاح.",
    UR: "میں تمام احتیاطی تدابیر اختیار کرنے کی تصدیق کرتا ہو  سماجی دوری کے لئے ، اور  سنی اوقاف کے قواعد شائع ہوئے جمعہ صلا. کے لئے۔",
    BN: "আমি সমস্ত সতর্কতা অবলম্বন নিশ্চিত সামাজিক দূরত্বের জন্য, এবং সুন্নি আওকাফের বিধি প্রকাশিত জুমা সালাহর জন্য"
  },


  // Drawer
  // ###############################################################################################################################

  my_profile: { EN: "My Profile", AR: "ملفي", UR: "میری پروفائل", BN: "আমার প্রোফাইল" },
  my_booking: { EN: "My Booking", AR: "الحجز", UR: "میری بکنگ", BN: "আমার বুকিং" },
  language: { EN: "Language", AR: "لغة", UR: "زبان", BN: "ভাষা" },
  invite_other: { EN: "Invite Other", AR: "دعوة أخرى", UR: "دوسرے کو مدعو کریں", BN: "আমন্ত্রণ করুন অন্য" },
  write_to_us: { EN: "Write to Us", AR: "اكتب لنا", UR: "ہمیں لکھیں", BN: "আমাদের লিখুন" },
  logout: { EN: "Logout", AR: "تسجيل خروج", UR: "لاگ آوٹ", BN: "প্রস্থান" },


  // If already booked
  // ###############################################################################################################################

  if_booked: {
    EN: "Sorry You can not Book two location for the same Salah",
    AR: "آسف لا يمكنك الحجز موقعين ل نفس صلاح",
    UR: "معذرت آپ بک نہیں کرسکتے ہیں کے لئے دو جگہ وہی صلاح",
    BN: "দুঃখিত আপনি বুক করতে পারবেন না জন্য দুটি অবস্থান একই সালাহ"
  },
  booked_msg: {
    EN: "You already booked a location in [Ahmed Masjid] For Juma’a Salah on [4 November 20]",
    AR: "لقد حجزت بالفعل أ الموقع في [مسجد أحمد] جمعة صلاح يوم [4 نوفمبر 20]",
    UR: "آپ نے پہلے ہی بک کیا [احمد مسجد] میں مقام جمعہ صلاح کیلئے [4 نومبر 20]",
    BN: "আপনি ইতিমধ্যে একটি বুক করেছেন [আহমেদ মসজিদে] অবস্থান জুম'আ সালাহ চালু আছে [২০ নভেম্বর ২০]"
  },
  ok: { EN: "Ok", AR: "حسنا", UR: "ٹھیک ہے", BN: "ঠিক আছে" },
  // Barcode messge
  // ###############################################################################################
  barcode_msg: {
    EN: "This is the barcode for your booking in [Masjid Name] For Juma’a Salah on [Date]",
    AR: "هذا هو الباركود الخاص بك الحجز في [Masjid Name] For جمعة صلاح في [التاريخ]",
    UR: "یہ آپ کے لئے بار کوڈ ہے [مسجد نام] میں بکنگ کیلئے جمعہ صلاح [تاریخ]",
    BN: "এটি আপনার জন্য বারকোড জন্য [মসজিদের নাম] বুকিং [তারিখ] এ জুম'আ সালাহ"
  },
  cancel_booking: { EN: "Cancel My Booking", AR: "إلغاء حجزي", UR: "میری بکنگ منسوخ کریں", BN: "আমার বুকিং বাতিল করুন" },




  // My Booking Screen
  // ###############################################################################################

  qr_scan_suggestion: { EN: "Scan this QR to enter Mosque", AR: "امسح رمز الاستجابة السريعة هذا للدخول إلى المسجد", UR: "مسجد میں داخل ہونے کے لئے اس QR کو اسکین کریں", BN: "মসজিদে প্রবেশের জন্য এই কিউআর স্ক্যান করুন" },
  get_direction: { EN: "Get Direction", AR: "احصل على اتجاه", UR: "سمت حاصل کریں", BN: "দিকনির্দেশ পান" },
  cancel_booking: { EN: "Cancel booking", AR: "إلغاء الحجز", UR: "بکنگ منسوخ کریں", BN: "বুকিং বাতিল করুন" },

  // Booking Confirmation Screen
  // ###############################################################################################

  booking_confirmation: { EN: "Booking confirmation", AR: "تأكيد الحجز", UR: "بکنگ کی تصدیق", BN: "বুকিং নিশ্চিতকরণ" },
  get_details: { EN: "Get Details", AR: "احصل على التفاصيل", UR: "تفصیلات حاصل کریں", BN: "বিস্তারিত বিবরণ পেতে" },
  confirmation_msg: {
    EN: "I Confirm taking all precautions for social distancing and the published rules of Sunni Awqaf for Juma’a Salah", AR: "أؤكد اتخاذ جميع الاحتياطات الخاصة بالتباعد الاجتماعي والقواعد المنشورة للأوقاف السنية لجمعة صلاح",
    UR: "میں جماع Sala صلاحہ کے لئے معاشرتی دوری اور سنی اوقاف کے شائع کردہ اصولوں سے متعلق تمام احتیاطی تدابیر اختیار کرنے کی تصدیق کرتا ہوں",
    BN: "আমি সামাজিক দূরত্বের জন্য সমস্ত সতর্কতা এবং জুম’আ সালাহর জন্য সুন্নি আওকাফের প্রকাশিত বিধিবিধানের বিষয়ে নিশ্চিত"
  },
  confirm_booking: { EN: "CONFIRM BOOKING", AR: "تأكيد الحجز", UR: "کنفرم بوکنگ", BN: "কনফার্ম বুকিং" },


//Masjid Information text
  // #################################################################################################################
  
  booking_status: { EN: "Booking Status", AR: "وضع الحجز", UR: "بکنگ کی حیثیت", BN: "বুকিংয়ের স্থিতি" },
  capacity: { EN: "Capacity", AR: "سعة", UR: "اہلیت", BN: "ক্ষমতা" },
  imam_name: { EN: "Imam name", AR: "اسم الإمام", UR: "امام نام", BN: "ইমামের নাম" },
  moazzin_name: { EN: "Moazzin name", AR: "اسم مؤذن", UR: "معززین نام", BN: "মোয়াজ্জিন নাম" },
  khadim_name: { EN: "Khadim name", AR: "اسم خادم", UR: "خادم نام", BN: "খাদিম নাম" },
  president: { EN: "President", AR: "رئيس", UR: "صدر", BN: "রাষ্ট্রপতি" },
  contact_number: { EN: " Contact number", AR: "رقم الاتصال", UR: "رابطہ نمبر", BN: "যোগাযোগের নম্বর" },
  email: { EN: "Email ", AR: "البريد الإلكتروني", UR: "ای میل", BN: "ইমেল" },
  about_the_mosque: { EN: "About the Mosque ", AR: "عن المسجد", UR: "مسجد کے بارے میں", BN: "মসজিদ সম্পর্কে" },
  more: { EN: "more... ", AR: "أكثر...", UR: "مزید...", BN: "আরও ..." },
  less: { EN: "less", AR: "أقل", UR: "کم", BN: "কম" },






  // 
  // password: { EN: "", AR: "", UR: "", BN: "" },

};

export default function (state = initialState, action) {
  return state;
}
