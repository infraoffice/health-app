import { combineReducers } from 'redux';
import indexAkshitaReducer from './indexAkshitaReducer ';
import indexReducer from './indexReducer';

export default combineReducers({
    indexReducer: indexReducer,
    indexAkshitaReducer:indexAkshitaReducer

})

