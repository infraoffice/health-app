import {
  
  SET_LOADING,

} from '../actions/types';

const initialState = {
  is_loading: false,
  current_lang: "EN",
  dropdown_placeholder: "English (EN)",

  is_dropdown: false,

  language_list: [
    {
      title: "English (EN)",
      value: 'EN'
    },

    {
      title: "Arabic (AR)",
      value: 'AR'
    },
    {
      title: "Urdu (UR)",
      value: 'UR'
    },
    {
      title: "Bengali (BN)",
      value: 'BN'
    },
  ]
};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_LOADING:
      return {
        ...state,
        is_loading: action.payload,
      };

  
    default:
      return state;
  }
}
