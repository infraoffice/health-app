
import {  SET_IS_READY, SET_LOGIN_MOBILE } from '../actions/typesAkshita';

const initialState = {
  is_loading: false,
  // login var

  login_mobile: '',
  is_ready: false,

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_LOGIN_MOBILE:
      return {
        ...state,
        login_mobile: action.payload,
      };
      case SET_IS_READY:
      return {
        ...state,
        is_ready: action.payload,
      };
   
  
    default:
      return state;
  }
}