
export const MODULE_KEY = 'module_app_store_';
// change width
export const SET_LOADING = MODULE_KEY + 'SET_LOADING';

// login mobile keys
export const SET_LOGIN_MOBILE = MODULE_KEY + 'SET_LOGIN_MOBILE';

export const SET_IS_READY = MODULE_KEY + 'SET_IS_READY';

