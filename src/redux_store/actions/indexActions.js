import store from '../store';
import {
    SET_IS_DROPDOWN,
} from './types';


export const setDropdownIsTrue = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_IS_DROPDOWN,
    payload: isTrue,
  });
};