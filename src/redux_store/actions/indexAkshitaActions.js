import store from '../store';

import {  SET_IS_READY, SET_LOGIN_MOBILE } from './typesAkshita';

export const setLoginMobile = (login_mobile) => (dispatch) => {
  console.log(login_mobile);
  dispatch({
    type: SET_LOGIN_MOBILE,
    payload: login_mobile,
  });
};

export const setIsReady = (is_ready) => (dispatch) => {
  dispatch({
    type: SET_IS_READY,
    payload: is_ready,
  });
};

