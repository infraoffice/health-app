import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Font from '../common/Font';
import Colors from '../common/Colors';
import Header from '../component/Header';
import { setIsReady } from '../redux_store/actions/indexAkshitaActions';
import * as Animatable from 'react-native-animatable';
import DrawerOption from '../component/DrawerOption';
import { getWidth } from '../common/Layout';
import ProfileComponent from '../component/ProfileComponent';

class HomeScreen extends Component {


  handleViewRef = (ref) => (this.view = ref);

  animate = () => {
    if (this.props.is_ready) {
      // this.setState({ ready: false });
      this.props.setIsReady(false)
      this.view.bounceOutLeft(2000);
    } else {
      // this.setState({ ready: true });
      this.props.setIsReady(false)
      this.view.bounceInLeft(2000);
    }
  };
  render() {

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.mainView}>

          <Header
            image1={require('../../assets/image/menu.png')}
            onPressMenu={() => this.props.setIsReady(!this.props.is_ready)}
            image2={require('../../assets/image/logo.png')}
            image3={require('../../assets/image/share.png')}
            image4={require('../../assets/image/logo.png')}
          />

        </View>


        {
          this.props.is_ready ?
            <TouchableOpacity
              onPress={() => {
                this.props.setIsReady(false)
              }}
              style={{
                position: 'absolute',
                height,
                width,
                elevation: 1,
                backgroundColor: "rgba(0, 0, 0, 0.28)"

              }}>

              <Animatable.View
                style={{
                  flex: 1,
                  alignSelf: 'flex-start',
                  backgroundColor: Colors.lightGray,
                  width: getWidth(297),
                  height,
                  elevation: 5,
                  backgroundColor: Colors.whiteColor,
                  position: 'absolute',
                  // marginTop: -40
                }}
                animation="bounceInLeft"
                ref={this.handleViewRef}>
                {/* <Header
                    onPress={() =>
                      this.props.setIsReady(false)
                    }
                  /> */}

                <ScrollView
                  showsHorizontalScrollIndicator={false}
                >
                  <ProfileComponent
                    profile={require('../../assets/image/profile.png')}
                    mobile_no={'+91 9966330033'}
                  />

                  <DrawerOption
                    icon={require('../../assets/image/invite.png')}
                    title={'Generate/Scan QR Code'}
                    onPress={() => { }}
                  />

                  <DrawerOption
                    icon={require('../../assets/image/invite.png')}
                    title={'Status Check'}
                    onPress={() => { }}
                  />

                  <DrawerOption
                    icon={require('../../assets/image/invite.png')}
                    title={'Approvals'}
                    onPress={() => { this.onLanguage() }}
                  />

                  <DrawerOption
                    icon={require('../../assets/image/invite.png')}
                    title={'Share Data with Govt'}
                    onPress={() => { }}
                  />

                  <DrawerOption
                    icon={require('../../assets/image/invite.png')}
                    title={'Call Helpline (115)'}
                    onPress={() => { }}
                  />

                  <DrawerOption
                    icon={require('../../assets/image/invite.png')}
                    title={'Settings'}
                    onPress={() => { }}
                  />
                    <DrawerOption
                    title={'Privacy Policy'}
                
                  />
                    <DrawerOption
                    title={'Terms of Use'}
                  />

                </ScrollView>
              </Animatable.View>
            </TouchableOpacity> :
            null
        }

      </SafeAreaView>
    );
  }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // height, width,
  },
  mainView: {
    flex: 1,
    backgroundColor: Colors.whiteColor
  },

  textStyle: {
    fontSize: 24,
    fontWeight: 'normal',
    fontFamily: Font.SourceSansPro,
  },
  imgStyle:
  {
    height: 150,
    width: 150
  }

});
const mapStateToProps = (state) => {
  const s = state.indexReducer;
  const stateAkshita = state.indexAkshitaReducer;

  return {

    is_ready: stateAkshita.is_ready,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setIsReady: (is_ready) => setIsReady(is_ready),
    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
