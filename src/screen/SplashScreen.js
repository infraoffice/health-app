import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('HomeScreen');
   
    }, 1000);
  }
  render() {

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <TouchableOpacity>
            <Image
              source={require('../../assets/icon/splash.png')}
              style={styles.imgStyle}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height,
    width,
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center'
  },

  textStyle: {
    fontSize: 24,
    fontWeight: 'normal',
    fontFamily: Font.SourceSansPro,
  },
  imgStyle:
  {
    height: 150,
    width: 150
  }

});
const mapStateToProps = (state) => {
  const s = state.indexReducer;

  return {
  
    // vendor_id: ss.vendor_id,

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
 

    },
    dispatch,
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
