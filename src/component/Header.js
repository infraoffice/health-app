import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import Colors from '../common/Colors';
import { getWidth } from '../common/Layout';


const Header = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={{ alignItems: 'center', }}
                onPress={props.onPressMenu}
            >
                <Image
                    style={[styles.imgStyle,]}
                    source={props.image1}
                />
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: 'center', }}>
                <Image
                    style={[styles.imgStyle,{marginRight:60}]}
                    source={props.image2}
                />
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: 'center', }}>
                <Image
                    style={[styles.imgStyle,{marginLeft:60}]}
                    source={props.image3}
                />
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: 'center', }}>
                <Image
                    style={styles.imgStyle}
                    source={props.image4}
                />
            </TouchableOpacity>

        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 55,
        backgroundColor: Colors.greenColor,
        alignItems: 'center',
        justifyContent: 'space-between',
    
    },
    imgStyle: {
        height: getWidth(20),
        width: getWidth(20)
    },
})
export default Header;
