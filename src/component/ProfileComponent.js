import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { Text } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { getWidth } from '../common/Layout';

const ProfileComponent = (props) => {
  return (
    <View style={styles.main}>

        <Image
          source={props.profile}
          style={styles.imgStyle}
        />
        <Text style={styles.txtStyle}>{props.mobile_no} </Text>
      </View>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: Colors.backgroundColor,
    height: 130,
    padding: 5,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  imgStyle: {
    height:30,
    width:25,
    // backgroundColor:'pink',
    borderRadius:100
  },
  txtStyle: {
    color: Colors.textColor,
    fontFamily: Font.SourceSansPro,
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 16,
    marginLeft:15
  }
});
export default ProfileComponent;
