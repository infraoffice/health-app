import SQLite from 'react-native-sqlite-storage';

const db = SQLite.openDatabase("test3.db", (success) => {
    console.log("database open success : " +success);
}, (error) => {
    console.log("Error : " + JSON.stringify(error));

});

export const executeSql = (sql,params=[]) => new Promise((resolve , reject)=>{
        db.transaction((tx)=>{
            tx.executeSql(sql, params, (db,results) => {
                    resolve(results);
                }, (error)=>{
                    reject(error);
                });
        });
    });