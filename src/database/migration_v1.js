import { executeSql } from './DatabaseExecutor';

const migration_name = `create_base_tables`;

export const run_migration_v1 = async () => {
  // await create_income();
};


const create_income = async (db) => {
  let table_name = 'income';
  let query = `create table if not exists ${table_name} (`;
  query += `income_id integer primary key AUTOINCREMENT,`;
  query += `income_amount integer,`;
  query += `income_business text, `;
  query += `income_date text, `;
  query += `income_time text, `;
  query += `income_description text, `;
  query += `income_payment_by text )`;

  try {
    let results = await executeSql(query, []);
    console.log(`created ${table_name} success`);
  } catch (error) {
    console.log(`creation error on ${table_name}` + JSON.stringify(error));
  }
};