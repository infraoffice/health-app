import { run_migration_v1 } from './migration_v1';
import { executeSql } from './DatabaseExecutor';
import { debugLog } from '../common/Constants';

export const run_database_migrations = async () => {
  console.log('running database migration');
  await run_migration_v1();
};


export const insertServiceList = async (serviceArray) => {
  const table_name = 'service_types';
  let query = `insert into ${table_name}`;
  query += `(service_name)`;
  query += `VALUES (?)`;
  // debugLog(serviceArray)

  try {
    serviceArray.map(async (item) => {
      let result = await executeSql(`select * from ${table_name} where service_name = '${item.item_name}'`,[]);
      debugLog("array list>>>")
      let array = result.rows;
      if (array.length === 0) {
        await executeSql(query, [item.item_name.toUpperCase()]);
        console.log('record inserted...');
      }
    });
    selectAllRecord(`SELECT * FROM ${table_name}`);
    console.log(`Record inserted successfully ${table_name} `);
  } catch (error) {
    console.log('Record is not inserted!');
  }
};

// Select record
export const selectAllRecord = async (query) => {
  let selectQuery = await executeSql(query, []);
  var rows = selectQuery.rows;
  var array = [];
  for (let i = 0; i < rows.length; i++) {
    array[i] = rows.item(i);
    // console.log(array[i]);
  }
  return array;
};

// Update Record
export const updateService = async (id, name) => {
  const table_name = 'users_name';
  let query = `update ${table_name} set name = '${name}' where id = ${id} `;

  try {
    await executeSql(query, []);
    console.log('user updated');
  } catch (error) {
    console.log(error);
  }
};

// Delete record
export const deleteService = async (id) => {
  const table_name = 'service_types';
  // let query = `delete from ${table_name} where id = ${id}`;
  let query = `delete from ${table_name}`;

  try {
    await executeSql(query, []);
    console.log(id + ' deleted!');
  } catch (error) {
    console.log(error);
  }
};