/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/redux_store/store';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Colors from './src/common/Colors';

import { run_database_migrations } from './src/database/AppDatabase';
import SplashScreen from './src/screen/SplashScreen';
import HomeScreen from './src/screen/HomeScreen';



// console.disableYellowBox = true;
const Stack = createStackNavigator();
class App extends Component {
  constructor(props) {
    super(props);
    run_database_migrations().then(() => {
      // console.log("creating table");
    });
  }
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar backgroundColor={Colors.backgroundColor}
            barStyle={"light-content"}
          />
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{ animationEnabled: false, headerShown: false }}
            >

              <Stack.Screen name="SplashScreen" component={SplashScreen} />
              <Stack.Screen name="HomeScreen" component={HomeScreen} />

            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;